const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const newInformaticsBuliding = await Building.findById('621b99b7e2651383d07c97fe')
  const room = await Room.findById('621b99b7e2651383d07c9802')
  const informaticsBuliding = await Building.findById(room.building)
  console.log(newInformaticsBuliding)
  console.log(room)
  console.log(informaticsBuliding)
  room.building = newInformaticsBuliding
  newInformaticsBuliding.rooms.push(room)
  informaticsBuliding.rooms.pull(room)
  room.save()
  newInformaticsBuliding.save()
  informaticsBuliding.save()
}

main().then(() => {
  console.log('Finish')
})
