const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  name: String,
  capacity: { type: Number, default: 50 },
  floor: Number,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
})

module.exports = mongoose.model('Room', roomSchema)
